﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour {

	[SerializeField] private float m_DeactivateTime = 10.0f;
	[SerializeField] private Vector3 m_RotateSpeed = new Vector3 (5,5,-5);

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		transform.Rotate (m_RotateSpeed);
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	private void OnEnable()
	{
		Invoke ("deactivateSelf",m_DeactivateTime);
	}

	/// <summary>
	/// Deactivates the self.
	/// </summary>
	private void deactivateSelf()
	{
		Destroy(this.gameObject);
	}

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	public void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Explosion")) {
			print ("Pickups Destroyed by Bomb");
			this.gameObject.SetActive (false);
		}
	}
}
