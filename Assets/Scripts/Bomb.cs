﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

	[SerializeField] GameObject explosion;
	[SerializeField] private float m_ExplodeDelay=3f;
	[SerializeField] private bool isExploded = false;

	public bool _IslongerBombblast=false;

	public LayerMask maskLayer;

	private System.Action m_CallBack=null;



	// Use this for initialization
	void Start () {
		Invoke ("explode",m_ExplodeDelay);
	}

	/// <summary>
	/// Explode this instance.
	/// </summary>
	private void explode()
	{
		isExploded = true;
		Instantiate (explosion, transform.position, explosion.transform.rotation);

		StartCoroutine (CreateLinearExplosion(Vector3.forward));
		StartCoroutine (CreateLinearExplosion(Vector3.back));
		StartCoroutine (CreateLinearExplosion(Vector3.left));
		StartCoroutine (CreateLinearExplosion(Vector3.right));

		if (m_CallBack != null) {
		
			m_CallBack.Invoke ();
			m_CallBack = null;
		}

		Destroy (this.gameObject,0.35f);


	}


	/// <summary>
	/// Sets the call back.
	/// </summary>
	/// <param name="callBack">Call back.</param>
	public void setCallBack(System.Action callBack)
	{
		m_CallBack = callBack;
	}

	/// <summary>
	/// Creates the linear explosion.
	/// </summary>
	/// <returns>The linear explosion.</returns>
	/// <param name="direction">Direction.</param>
	private IEnumerator CreateLinearExplosion(Vector3 direction)
	{
		int longExlosionCount=0;

		if (_IslongerBombblast)
			longExlosionCount = 2;	

		for (int idx = 1; idx < 3+longExlosionCount; idx++) {
		
			RaycastHit hit;

			Physics.Raycast (transform.position+new Vector3(0,0.5f,0),direction,out hit,idx,maskLayer);



			if (!hit.collider) {
			
				Instantiate (explosion, transform.position + direction * idx, explosion.transform.rotation);
			}
			else if(hit.collider.tag == "GrassWall")
			{
				hit.collider.gameObject.SetActive(false);
			}
			else
				break;
		}

		yield return new WaitForSeconds (0.5f);
		
	}
	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="otherCol">Other col.</param>

	public void OnTriggerEnter ( Collider otherCol)
	{
		if (!isExploded && otherCol.CompareTag ("Explosion")) {
		
			CancelInvoke ("explode");

			explode ();
		
		}

	}



}
