﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
	#region Public Vars
    //Player parameters
    [Range (1, 2)] //Enables a nifty slider in the editor
    public int playerNumber 	= 1;
    //Indicates what player this is: P1 or P2
    public float moveSpeed 		= 5f;
	public float runSpeed 		= 8f;
	public float currentSpeed	= 5f;
    public bool canDropBombs 	= true;
    //Can the player drop bombs?
    public bool canMove 		= true;

    //Can the player move?
	public List<PICKUPS> pickUpsList = new List<PICKUPS>();
	public bool isDead 			= false;
	// Is player dead??


	//Prefabs
	public GameObject bombPrefab;
	public GameObject playerPrefab;
	#endregion


	#region Private Vars

   
    
    private Rigidbody rigidBody;
    private Transform myTransform;
    private Animator animator;

	//Amount of bombs the player has left to drop, gets decreased as the player
	//drops a bomb, increases as an owned bomb explodes
	//Cached components
	private int m_PlayerBombCount=1;

	private bool isLongerBombBlast=false,isRemoteControlled=false,isFastSpeed=false;

	#endregion

	#region Private Method

    // Use this for initialization
    void Start ()
    {
 
		GameController.OnReplay.AddListener (onReplay);
        rigidBody = GetComponent<Rigidbody> ();
        myTransform = transform;
		animator = myTransform.Find (GUtils.getPlayerPrefabName(playerNumber)).GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update ()
    {
        UpdateMovement ();
    }

	/// <summary>
	/// Deactivate the player prefab.
	/// </summary>
	private void DestroyGO()
	{
		if(isDead)
		playerPrefab.SetActive (false);
	}

	/// <summary>
	/// Calls on replay, registered as listener of replay event in game controller.
	/// </summary>
	private void onReplay()
	{
		

		canMove = true;
		isDead = false;
		canDropBombs = true;

		transform.localPosition = GUtils.getPlayerInitPos (playerNumber);
		playerPrefab.SetActive (true);
		animator.Play ("Idle 1");


	}

	/// <summary>
	/// Updates the movement.
	/// </summary>

    private void UpdateMovement ()
    {
		
		if (!canMove || isDead)
        { //Return if player can't move
            return;
        }

		animator.SetBool ("Walking", false);
        //Depending on the player number, use different input for moving

		if(!GameController.Instance.isGameEnd)
		{
	        if (playerNumber == 1)
	        {
	            UpdatePlayer1Movement ();
	        } else
	        {
	            UpdatePlayer2Movement ();
	        }
		}
    }

    /// <summary>
    /// Updates Player 1's movement and facing rotation using the WASD keys and drops bombs using Space
    /// </summary>
    private void UpdatePlayer1Movement ()
    {
		
		if (Input.GetKey (KeyCode.Q)  && isFastSpeed ) {
			currentSpeed = runSpeed;
		} else
			currentSpeed = moveSpeed;

        if (Input.GetKey (KeyCode.W))
        { //Up movement
			rigidBody.velocity = new Vector3 (rigidBody.velocity.x, rigidBody.velocity.y, currentSpeed );
            myTransform.rotation = Quaternion.Euler (0, 0, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.A))
        { //Left movement
			rigidBody.velocity = new Vector3 (-currentSpeed , rigidBody.velocity.y, rigidBody.velocity.z);
            myTransform.rotation = Quaternion.Euler (0, 270, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.S))
        { //Down movement
			rigidBody.velocity = new Vector3 (rigidBody.velocity.x, rigidBody.velocity.y, -currentSpeed );
            myTransform.rotation = Quaternion.Euler (0, 180, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.D))
        { //Right movement
			rigidBody.velocity = new Vector3 (currentSpeed , rigidBody.velocity.y, rigidBody.velocity.z);
            myTransform.rotation = Quaternion.Euler (0, 90, 0);
            animator.SetBool ("Walking", true);
        }

        if (canDropBombs && Input.GetKeyDown (KeyCode.Space))
        { //Drop bomb
            DropBomb ();
        }
    }

    /// <summary>
    /// Updates Player 2's movement and facing rotation using the arrow keys and drops bombs using Enter or Return
    /// </summary>
    private void UpdatePlayer2Movement ()
    {

		if (Input.GetKey (KeyCode.RightControl) && isFastSpeed) {
			currentSpeed = runSpeed;
		} else
			currentSpeed = moveSpeed;
		
        if (Input.GetKey (KeyCode.UpArrow))
        { //Up movement
			rigidBody.velocity = new Vector3 (rigidBody.velocity.x, rigidBody.velocity.y, currentSpeed );
            myTransform.rotation = Quaternion.Euler (0, 0, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.LeftArrow))
        { //Left movement
			rigidBody.velocity = new Vector3 (-currentSpeed , rigidBody.velocity.y, rigidBody.velocity.z);
            myTransform.rotation = Quaternion.Euler (0, 270, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.DownArrow))
        { //Down movement
			rigidBody.velocity = new Vector3 (rigidBody.velocity.x, rigidBody.velocity.y, -currentSpeed );
            myTransform.rotation = Quaternion.Euler (0, 180, 0);
            animator.SetBool ("Walking", true);
        }

        if (Input.GetKey (KeyCode.RightArrow))
        { //Right movement
			rigidBody.velocity = new Vector3 (currentSpeed , rigidBody.velocity.y, rigidBody.velocity.z);
            myTransform.rotation = Quaternion.Euler (0, 90, 0);
            animator.SetBool ("Walking", true);
        }

        if (canDropBombs && (Input.GetKeyDown (KeyCode.KeypadEnter) || Input.GetKeyDown (KeyCode.Return)))
        { //Drop Bomb. For Player 2's bombs, allow both the numeric enter as the return key or players 
            //without a numpad will be unable to drop bombs
            DropBomb ();
        }
    }

    /// <summary>
    /// Drops a bomb beneath the player
    /// </summary>
	/// 
    private void DropBomb ()
    {
		if (bombPrefab && m_PlayerBombCount>0)
        { 
			//Check if bomb prefab is assigned first
			GameObject go= Instantiate(bombPrefab, new Vector3(Mathf.RoundToInt(transform.position.x),transform.position.y,Mathf.RoundToInt(transform.position.z)), bombPrefab.transform.rotation);
			m_PlayerBombCount--;

			if(isLongerBombBlast)
			go.GetComponent<Bomb> ()._IslongerBombblast = true;

			go.GetComponent<Bomb> ().setCallBack (()=>{
				m_PlayerBombCount++;
			});
		}
    }

	#endregion

	#region Public Methods

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
    public void OnTriggerEnter (Collider other)
    {
		if (other.CompareTag ("Explosion")) {
			if (!isDead) {
				isDead = true;
				GameController.Instance.PlayerDied (playerNumber);
				transform.GetComponentInChildren<Animator> ().Play ("Death");

				Invoke ("DestroyGO", 2.5f);
				Debug.Log ("P" + playerNumber + " hit by explosion!");
			}

		} 
		else if (other.CompareTag ("FastRun")) {
		
			print ("Fast Run Collected");

			other.gameObject.SetActive (false);
			OnCollisionWithPickups (PICKUPS.FAST_RUN);


		}  
		else if (other.CompareTag ("LongBomb")) {

			print ("Long Bomb Collected");
			other.gameObject.SetActive (false);
			OnCollisionWithPickups (PICKUPS.LONG_BOMB_BLAST);

		}  
		else if (other.CompareTag ("RemoteBomb")) {

			print ("Remote Bomb Collected");
			other.gameObject.SetActive (false);
			OnCollisionWithPickups (PICKUPS.REMOTE_CONTROL);

		} 
		else if (other.CompareTag ("MoreBomb")) {

			print ("More Bomb collected");
			other.gameObject.SetActive (false);
			OnCollisionWithPickups (PICKUPS.MORE_BOMBS);

		}
    }


	/// <summary>
	/// Raises the collision with pickups event.
	/// </summary>
	/// <param name="pickups">Pickups.</param>
	public void OnCollisionWithPickups(PICKUPS pickups)
	{

		pickUpsList.Add (pickups);
		GameController.Instance.updateScoreField ();
		switch(pickups)
		{

		case PICKUPS.FAST_RUN:
			{
				isFastSpeed = true;
			}
			break;
		case PICKUPS.LONG_BOMB_BLAST:
			{
				isLongerBombBlast = true;
			}
			break;
		case PICKUPS.MORE_BOMBS:
			{
				m_PlayerBombCount++;
			}
			break;
		case PICKUPS.REMOTE_CONTROL:
			{
				isRemoteControlled = true;
			}
			break;

		}

		StartCoroutine(removePickUpsEffect (pickups, 10));
	}


	/// <summary>
	/// Removes the pick ups effect.
	/// </summary>
	/// <returns>The pick ups effect.</returns>
	/// <param name="pickups">Pickups.</param>
	/// <param name="delay">Delay.</param>
	private IEnumerator removePickUpsEffect(PICKUPS pickups, float delay)
	{
		

		yield return new WaitForSeconds (delay);

		print ("pickups Removed "+pickups.ToString());

		pickUpsList.Remove (pickups);
		GameController.Instance.updateScoreField ();
		switch(pickups)
		{

		case PICKUPS.FAST_RUN:
			{
				isFastSpeed = false;
			}
			break;
		case PICKUPS.LONG_BOMB_BLAST:
			{
				isLongerBombBlast = false;
			}
			break;
		case PICKUPS.MORE_BOMBS:
			{
				m_PlayerBombCount--;
			}
			break;
		case PICKUPS.REMOTE_CONTROL:
			{
				isRemoteControlled = false;
			}
			break;

		}


	}

	#endregion
}

/// <summary>
/// PICKUP.
/// </summary>
public enum PICKUPS
{
	LONG_BOMB_BLAST,
	MORE_BOMBS,
	FAST_RUN,
	REMOTE_CONTROL
}
