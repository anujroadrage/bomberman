﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapClass : MonoBehaviour {

	public static MapClass Instance = null;

	#region private Vars


	/// The width and Height of Arena.
	[Range(10,15)]
	[SerializeField] private int 		m_WidthOfMap 		=	10;
	[Range(10,15)]
	[SerializeField] private int 		m_LenghtOfMap 		=	10;

	// Floor Prefab obejct create floor and the parent container
	[SerializeField] private GameObject m_FloorPrefab 		= 	null;
	[SerializeField] private Transform 	m_FloorContainer 	= 	null;

	// Boundary Prefab object create Boundary and Its parent container
	[SerializeField] private GameObject m_BoundaryPrefab 	= 	null;
	[SerializeField] private Transform 	m_BoundrayContainer = 	null;

	// Grass Wall and its container
	[SerializeField] private GameObject m_BlockGrass 		= 	null;
	[SerializeField] private Transform 	m_BlockContainer 	= 	null;


	// Map Arena Wall are pooled so that that can be loaded in replay without instantiating objet again
	private List<GameObject> blocksPool = new List<GameObject>();


	// Map for walls, 0 = Empty, 1= solid wall, 2= grass wall this is hardcoded. This map will be randomized in future
	short[,] map = new short[10,10]{
		{0,0,0,0,0,0,0,0,0,0},
		{0,1,0,1,0,1,2,1,1,0},
		{0,0,0,1,0,0,0,0,2,0},
		{0,1,0,1,2,1,0,0,2,0},
		{0,2,0,0,0,0,0,0,1,0},
		{0,1,0,2,0,1,0,0,0,0},
		{0,0,0,2,0,2,1,2,1,0},
		{0,1,0,1,0,2,0,0,2,0},
		{0,2,0,1,0,1,0,1,2,0},
		{0,0,0,0,0,0,0,0,0,0}
	};


	#endregion

	#region Private Methods
	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy (this.gameObject);


		if(m_FloorContainer == null)
			m_FloorContainer = transform.Find ("Floor");   

		m_WidthOfMap = Mathf.Clamp (m_WidthOfMap,10,15);
		m_LenghtOfMap = Mathf.Clamp (m_LenghtOfMap,10,15);

		// Creating floor, boundary and Map
		// This will be replaced by procedural mesh later

		createFloor ();

		createBoundary ();

		createMap ();


	}



	private void createFloor()
	{
		for (int idx = 0; idx < m_WidthOfMap; idx++) {

			for (int jdx = 0; jdx < m_LenghtOfMap; jdx++) {

				GameObject go = Instantiate (m_FloorPrefab);
				go.transform.SetParent (m_FloorContainer);
				go.transform.localPosition = new Vector3 (idx,-0.5f,jdx);

				if((idx+jdx)%2 == 0)
					go.GetComponent<Renderer> ().material.color = Color.black;

			}

		}

	}

	private void createBoundary()
	{
		for (int idx = -1; idx <= m_WidthOfMap; idx++) {

			GameObject go = Instantiate (m_BoundaryPrefab);
			GameObject go1 = Instantiate (m_BoundaryPrefab);

			go.transform.SetParent (m_BoundrayContainer);
			go1.transform.SetParent (m_BoundrayContainer);

			go.transform.localPosition = new Vector3 (idx,0.0f,-1);
			go1.transform.localPosition = new Vector3 (idx,0.0f,m_WidthOfMap);

		}

		for (int jdx = 0; jdx < m_WidthOfMap; jdx++) {

			GameObject go = Instantiate (m_BoundaryPrefab);
			GameObject go1 = Instantiate (m_BoundaryPrefab);

			go.transform.SetParent (m_BoundrayContainer);
			go1.transform.SetParent (m_BoundrayContainer);

			go.transform.localPosition = new Vector3 (-1,0.0f,jdx);
			go1.transform.localPosition = new Vector3 (m_LenghtOfMap,0.0f,jdx);

		}

	}

	private void createMap()
	{

		for (int idx = 0; idx < m_WidthOfMap; idx++) {

			for (int jdx = 0; jdx < m_LenghtOfMap; jdx++) {

				if (map.GetLength(0)<=idx || map.GetLength(1)<=jdx)
					continue;

				if (map [idx,jdx] == 1) {
					GameObject go = Instantiate (m_BoundaryPrefab);
					go.transform.SetParent (m_BlockContainer);
					go.transform.localPosition = new Vector3 (idx, 0.0f, jdx);
					blocksPool.Add (go);

				} else if (map [idx,jdx] == 2) {
					GameObject go = Instantiate (m_BlockGrass);
					go.transform.SetParent (m_BlockContainer);
					go.transform.localPosition = new Vector3 (idx, 0.0f, jdx);				
					blocksPool.Add (go);
				}
					
			}

		}


	}

	#endregion

	#region Public Methods

	/// <summary>
	/// Gets the pickups position to instantiate Pickups.
	/// Check for empty area
	/// </summary>
	/// <returns>The pickups position.</returns>

	public Vector3 getPickupsPosition()
	{
		int widthPos 	= Random.Range (0,map.GetLength(0));
		int lenghtPos 	= Random.Range (0,map.GetLength(1));

		while (map [widthPos, lenghtPos] != 0) {
		
			widthPos 	= Random.Range (0,map.GetLength(0));
			lenghtPos 	= Random.Range (0,map.GetLength(1));


		}

		return new Vector3 (widthPos,0,lenghtPos);

		return Vector3.zero;
	}


	/// <summary>
	/// Recreates the map on replay.
	/// </summary>
	/// 
	public void RecreateMap()
	{
		foreach(GameObject go in blocksPool)
		{
			go.SetActive (true);
		}
	}

	#endregion


}
