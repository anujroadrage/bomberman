﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {

	public static CanvasController Instance = null;
	[SerializeField] GameObject m_BG;
	[SerializeField] GameObject m_ResultPanel;
	[SerializeField] GameObject m_ScoreP1,m_ScoreP2;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy (this.gameObject);

		m_ScoreP1.GetComponentInChildren<Text> ().text = "P1 Win : 0";
		m_ScoreP2.GetComponentInChildren<Text> ().text = "P2 Win : 0";
	}


	public void updateScorePanel(int player1Win, int player2Win)
	{

		m_ScoreP1.GetComponentInChildren<Text> ().text = "";
		m_ScoreP2.GetComponentInChildren<Text> ().text = "";

		m_ScoreP1.GetComponentInChildren<Text> ().text = "P1 Win : "+player1Win.ToString();
		foreach (PICKUPS pickup in GameController.Instance.player1.pickUpsList) {
			m_ScoreP1.GetComponentInChildren<Text> ().text += "," + pickup.ToString ();
		}


		m_ScoreP2.GetComponentInChildren<Text> ().text = "P2 Win : " + player2Win.ToString ();
		foreach (PICKUPS pickup in GameController.Instance.player2.pickUpsList) {
			m_ScoreP2.GetComponentInChildren<Text> ().text += "," + pickup.ToString ();
		}
			
	}

	public void showResult(int player1Win, int player2Win, int wonPlayerNum)
	{
		m_ResultPanel.SetActive (true);
		m_BG.SetActive (true);
		if(wonPlayerNum==0)
			m_ResultPanel.transform.Find ("Result").GetComponent<Text>().text = "Game Tied";
		else
			m_ResultPanel.transform.Find ("Result").GetComponent<Text>().text = "Player "+wonPlayerNum.ToString()+" Won";

		m_ResultPanel.transform.Find ("Player1").GetComponent<Text>().text = "Player1 Winnings : "+player1Win.ToString();
		m_ResultPanel.transform.Find ("Player2").GetComponent<Text>().text = "Player2 Winnings : "+player2Win.ToString();


		updateScorePanel (player1Win,player2Win);

	}

	public void OnReplayClicked()
	{
		m_BG.SetActive (false);
		m_ResultPanel.SetActive (false);
		GameController.Instance.RePlay ();
	}



}
