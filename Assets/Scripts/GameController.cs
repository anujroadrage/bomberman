﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameController : MonoBehaviour {


	public static GameController Instance	=	null;

	/// <summary>
	/// The Replay Event to invoke methods on Replay.
	/// </summary>
	public static UnityEvent OnReplay = new UnityEvent();


	#region Public Vars

	/// <summary>
	/// The Player's prefab.
	/// </summary>
	public Player player1;
	public Player player2;

	#endregion


	#region Private Vars

	/// <summary>
	/// The Pickups 
	/// </summary>
	[SerializeField] private GameObject[] m_Pickups;
	[SerializeField] private float 	m_PickupsTimeDelay=20f;
	[SerializeField] private Text 	m_Timer;
	[SerializeField] private float 	m_TimeVal=300f;

	private int m_FirstPlayerWin 	= 0;
	private int m_SecondPlayerWin 	= 0;

	private int m_DeadPlayerCount	= 0;
	private int m_DeadPlayerNum 	= 0;

	public bool isGameEnd			= false;

	private int min, sec;
	#endregion

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy (this.gameObject);
	}

	void Start()
	{
		InvokeRepeating ("initPickups",10.0f,m_PickupsTimeDelay);
		startTimer ();
	}


	/// <summary>
	/// Methods called when Players the died with id number as param.
	/// </summary>
	/// <param name="playerNumber">Player number.</param>
	public void PlayerDied (int playerNumber)
	{
		
		m_DeadPlayerNum = playerNumber;
		m_DeadPlayerCount++;

		if (isGameEnd)
			return;

		isGameEnd = true;

		CancelInvoke ("initPickups");
		Invoke ("CheckPlayerWin",0.4f);

	}

	/// <summary>
	/// Ends the game.
	/// </summary>

	public void EndGame()
	{
	
		isGameEnd = true;
		CancelInvoke ("initPickups");
		Invoke ("CheckPlayerWin",0.1f);

	}

	/// <summary>
	/// Updates the score field.
	/// </summary>

	public void updateScoreField()
	{
		CanvasController.Instance.updateScorePanel (m_FirstPlayerWin,m_SecondPlayerWin);

	}

	/// <summary>
	/// called to replay.
	/// </summary>
	public void RePlay()
	{
		print ("RePlayed ");

		m_DeadPlayerCount 		= 0;
		m_DeadPlayerNum 		= 0;
		isGameEnd 				= false;
		OnReplay.Invoke ();
		startTimer ();
		InvokeRepeating ("initPickups",10.0f,10.0f );
		MapClass.Instance.RecreateMap ();
	}


	/// <summary>
	/// Checks the player wins, calls after a delay when a player died.
	/// </summary>

	private void CheckPlayerWin()
	{
		if (m_DeadPlayerCount == 1) {

			if (m_DeadPlayerNum == 1) {
				m_SecondPlayerWin++;
			} else {
				m_FirstPlayerWin++;
			}
				CanvasController.Instance.showResult (m_FirstPlayerWin,m_SecondPlayerWin,m_DeadPlayerNum==1?2:1);
		

		} else {
			
			CanvasController.Instance.showResult (m_FirstPlayerWin,m_SecondPlayerWin,0);

		}
	}

	/// <summary>
	/// Inits the pickups.
	/// </summary>
	/// <param name="initPos">Init position.</param>

	public void initPickups(Vector3 initPos)
	{
		print ("Init Pickups With Position");

		int temp = Random.Range (0,int.MaxValue)%100;

		if (temp < 30) {
			int idx = Random.Range (0, m_Pickups.Length);

			if (m_Pickups [idx] != null) {
				GameObject go = Instantiate (m_Pickups [idx], initPos, m_Pickups [idx].transform.rotation);

				//Destroy (go.GetComponent<PickUps>());
				go.SetActive (true);
				StartCoroutine (addComp (go));
			}

		}
	}

	/// <summary>
	/// Adds the component after a delay to save pickups being destroyed from bomb explosion.
	/// </summary>
	/// <returns>The comp.</returns>
	/// <param name="go">Go.</param>
	private  IEnumerator addComp(GameObject go)
	{
		yield return new WaitForSeconds (1.0f);
		go.AddComponent<PickUps> ().enabled = true;

	}

	/// <summary>
	/// Starts the timer.
	/// </summary>
	private void startTimer()
	{
	
		m_TimeVal = 300;
		InvokeRepeating ("updateTimer",0.0f,1.0f);

	}

	/// <summary>
	/// Updates the timer.
	/// </summary>
	private void updateTimer()
	{
		getMinSec (m_TimeVal--);
		m_Timer.text = (min > 9 ? "" : "0") + min.ToString ()+":" + (sec > 9 ? "" : "0") + sec.ToString ();
	
		if (m_TimeVal < 0) {
			CancelInvoke ("updateTimer");

			EndGame ();
		
		}
	}

	/// <summary>
	/// update mminutes and seconds left to ends the game.
	/// </summary>
	/// <param name="time">Time.</param>
	private void getMinSec(float time)
	{
		min = (int)(time / 60);
		sec = (int)(time % 60);
	}

	/// <summary>
	/// Inits the pickups.
	/// </summary>
	private void initPickups()
	{
		print ("Init Pickups");

		int idx = Random.Range (0,m_Pickups.Length);

		GameObject go = Instantiate(m_Pickups [idx],MapClass.Instance.getPickupsPosition (),m_Pickups[idx].transform.rotation);
		//pickups [idx].transform.localPosition = MapClass.Instance.getPickupsPosition ();

		go.SetActive(true);
		go.AddComponent<PickUps> ().enabled = true;


		//pickups [idx].SetActive (true);

	}




}
