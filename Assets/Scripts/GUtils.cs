﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUtils {

	/// <summary>
	/// Gets the player init position.
	/// </summary>
	/// <returns>The player init position.</returns>
	/// <param name="playerIdx">Player index.</param>
	public static Vector3 getPlayerInitPos(int playerIdx)
	{
		switch(playerIdx)
		{

		case 1:
			return new Vector3 (0.5f,0,9);
		case 2:
			return new Vector3 (9,0,0.5f);

		}

		return Vector3.zero;
		
	}
	/// <summary>
	/// Gets the name of the player prefab.
	/// </summary>
	/// <returns>The player prefab name.</returns>
	/// <param name="playerIdx">Player index.</param>

	public static string getPlayerPrefabName(int playerIdx)
	{
		switch(playerIdx)
		{

		case 1:
			return "FreeVoxelGirlRedHair";
		case 2:
			return "FreeVoxelGirlBlackHair";

		}

		return "";

	}

	/// <summary>
	/// Gets the name of the pickups.
	/// </summary>
	/// <returns>The pickups name.</returns>
	/// <param name="pickups">Pickups.</param>
	public static string getPickupsName (PICKUPS pickups)
	{
		switch (pickups) {

		case PICKUPS.FAST_RUN:
			return "FastRun";
		case PICKUPS.LONG_BOMB_BLAST:
			return "LongBombBlast";
		case PICKUPS.MORE_BOMBS:
			return "MoreBomb";
		case PICKUPS.REMOTE_CONTROL:
			return "RemoteControl";
		}

		return "";
	}



}
